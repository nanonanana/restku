import RPi.GPIO as GPIO
import time

class Solenoid:
	def __init__(self, data):
		self.data = data
		self.port = 13

	def init(self):
		GPIO.setmode(GPIO.BOARD)
		GPIO.setwarnings(False)
		GPIO.setup(self.port, GPIO.OUT)

	def action(self):
		GPIO.output(self.port, self.data)

	def setLockMode(self):
		self.data = 1

	def setUnlockMode(self):
		self.data = 0
	
