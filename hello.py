from solenoid import Solenoid
from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/kunci", methods=["POST"])
def test() :
	if request.form['data'] == "1":
		queueTest(1)
	else:
		queueTest(0)
				
	return jsonify({'data': request.form['data']})


def queueTest(mode) :
	sol = Solenoid(mode)
	sol.init()
	sol.action()

if __name__ == "__main__":
	app.debug = True
	app.run(host='0.0.0.0')
